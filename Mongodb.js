// Importer le driver MongoDB
const { MongoClient } = require('mongodb');

// Connexion à la base de données
const uri = "mongodb://localhost:27017";
const client = new MongoClient(uri);

async function main() {
  try {
    await client.connect();

    // 1. Création de la base de données "contact" et de la collection "liste de contacts"
    const db = client.db("contact");
    const collection = db.collection("liste de contacts");

    // 2. Insertion des documents dans la collection "liste de contacts"
    const documents = [
      { Nom: "Ben", Prenom: "Moris", Email: "ben@gmail.com", age: 26 },
      { Nom: "Kefi", Prenom: "Seif", Email: "kefi@gmail.com", age: 15 },
      { Nom: "Emilie", Prenom: "brouge", Email: "emilie.b@gmail.com", age: 40 },
      { Nom: "Alex", Prenom: "brown", age: 4 },
      { Nom: "Denzel", Prenom: "Washington", age: 3 }
    ];
    await collection.insertMany(documents);

    // 3. Afficher toute la liste des contacts
    const allContacts = await collection.find().toArray();
    console.log("Tous les contacts :", allContacts);

    // 4. Afficher les informations d'un contact en utilisant son identifiant
    const contactId = allContacts[0]._id;
    const oneContact = await collection.findOne({ _id: contactId });
    console.log("Informations du premier contact :", oneContact);

    // 5. Afficher tous les contacts avec un âge > 18
    const adulteContacts = await collection.find({ age: { $gt: 18 } }).toArray();
    console.log("Contacts adultes :", adulteContacts);

    // 6. Afficher tous les contacts avec un âge > 18 et un nom contenant "ah"
    const adulteContactsWithAh = await collection.find({ age: { $gt: 18 }, Nom: /ah/ }).toArray();
    console.log("Contacts adultes avec 'ah' dans le nom :", adulteContactsWithAh);

    // 7. Modifier le prénom du contact "Kefi Seif" en "Kefi Anis"
    await collection.updateOne({ Prenom: "Seif", Nom: "Kefi" }, { $set: { Prenom: "Anis" } });

    // 8. Supprimer les contacts âgés de moins de 5 ans
    await collection.deleteMany({ age: { $lt: 5 } });

    // 9. Afficher toute la liste des contacts mise à jour
    const updatedContacts = await collection.find().toArray();
    console.log("Tous les contacts mis à jour :", updatedContacts);
  } catch (err) {
    console.error("Une erreur est survenue :", err);
  } finally {
    await client.close();
  }
}

main();